const express = require('express');
const router = express.Router();

const Account = require("../models/account.model");
const Authenticator = require("../utils/authenticator.util");

const NotificationUtil = require("../utils/notification.util");
const UserActivityUtil = require("../utils/userActivity.util");

const authenticate = (req, res, next) => {
  Authenticator.authenticate(req.headers.idtoken).then((response) => {
    res.locals.user = response;
    req.authUsername = response.username;
    next();
  }).catch((error) => {
    if (error.code === 0) {
      res.status(200).json({
          statusCode: 505,
          timestamp: Date.now(),
          message: "idToken is not valid",
          data: {
              code: "client_not_authenticated"
          }
      });
    }
  });
};

const authenticateApp = (req, res, next) => {
  Authenticator.authenticateApp(req.headers.authtoken).then((response) => {
      if (response.authenticated === true) {
          next();
      }
  }).catch((error) => {
      if (error.authenticated === false) {
          res.status(200).json({
              statusCode: 505,
              timestamp: Date.now(),
              message: "authtoken is not valid",
              data: {
                  code: "server_not_authenticated"
              }
          });
      }
  });
}



router.get('/:username', [authenticate, authenticateApp], (req, res) => {
    NotificationUtil.sendSlackMessage("14: application is getting account info for username: "+req.params.username, "CF3U0LPDF");
    UserActivityUtil.logActivity("fullAccountDataRetrieval", req.body.ipData, req.body.username);
    Account.find({'username': req.params.username}).exec((error, account) => {
        if (error) {
            NotificationUtil.sendSlackMessage("505: error during GET /account/:username", "CF3U0LPDF");
            res.status(202).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: 'operation failed',
                data: error
            });
        }
        else {
            res.status(202).json({
                statusCode: 202,
                timestamp: Date.now(),
                message: 'operation passed',
                data: account
            });
        }
    });
});

// router.post('/', [authenticate, authenticateApp], (req, res) => {
//   Account.findByIdAndUpdate(req.body.id, req.body.account).exec((error, newAccount) => {
//       if (error) {
//         res.status(202).json({
//             statusCode: 505,
//             timestamp: Date.now(),
//             message: 'operation failed',
//             data: error
//         });
//       }
//       else {
//         res.status(202).json({
//             statusCode: 202,
//             timestamp: Date.now(),
//             message: 'account is updated',
//             data: account
//         });
//     }
//   });
// });

module.exports = router;

const router = require('express').Router();

const AccountUtil = require('../utils/account.util');
const AccountModel = require('../models/account.model');
const VerificationCodeUtil = require("../utils/verificationCode.util");

const NotificationUtil = require("../utils/notification.util");
const UserActivityUtil = require("../utils/userActivity.util");

router.get("/check_username/:username", (req, res) => {
    AccountUtil.checkForUsername(req.params.username.toLowerCase()).then((result) => {
        res.status(200).json({
            statusCode: 200,
            timestamp: Date.now(),
            message: "operation passed",
            data: {
                result: result
            }
        });
    }).catch((error) => {
        NotificationUtil.sendSlackMessage("505: error during /help/check_username", "CF3U0LPDF");
        res.status(200).json({
            statusCode: 505,
            timestamp: Date.now(),
            message: "operation failed.",
            data: error
        });
    });
});

router.get("/get_username/:email", (req, res) => {
    AccountUtil.getUsernameWithEmail(req.params.email).then((result) => {
        if (result.code === "no_account") {
            NotificationUtil.sendSlackMessage("505: error during /help/get_username", "CF3U0LPDF");
            res.status(200).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: "operation failed.",
                data: result
            });
        }
        else {
            res.status(200).json({
                statusCode: 200,
                timestamp: Date.now(),
                message: "operation passed.",
                data: result
            });
        }
    }).catch((error) => {
        NotificationUtil.sendSlackMessage("505: error during /help/get_username", "CF3U0LPDF");
        res.status(200).json({
            statusCode: 505,
            timestamp: Date.now(),
            message: "operation failed.",
            data: error
        });
    });
});

router.get("/check_email/:email", (req, res) => {
    AccountUtil.checkForEmail(req.params.email).then((result) => {
        res.status(200).json({
            statusCode: 200,
            timestamp: Date.now(),
            message: "operation passed",
            data: {
                result: result
            }
        });
    }).catch((error) => {
        NotificationUtil.sendSlackMessage("505: error during /help/check_email", "CF3U0LPDF");
        res.status(200).json({
            statusCode: 505,
            timestamp: Date.now(),
            message: "operation failed.",
            data: error
        });
    });
});

router.get("/reset_pass/:username", (req, res) => {
    NotificationUtil.sendSlackMessage("9: resetting password started for username:"+req.params.username, "CF3U0LPDF");
    UserActivityUtil.logActivity("passwordResetRequest", null, req.params.username);
    AccountModel.findOne({'username': req.params.username}).exec((err, account) => {
        if (err) {
            NotificationUtil.sendSlackMessage("505: error during /help/reset_pass", "CF3U0LPDF");
            res.status(202).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: 'finding user information based on username failed',
                data: err
            });
        }
        else {
            VerificationCodeUtil.createCode(req.params.username).then((codeData) => {
                VerificationCodeUtil.sendCode("phone", codeData.code, account.phone, codeData.username, "Hey USERNAME,\n\nYour verification code is CODE to sign in to reset your TCF Account password\n\n- Jojo the Unicorn").then((response) => {
                    res.status(202).json({
                        statusCode: 202,
                        timestamp: Date.now(),
                        message: "code is sent for verifying the user",
                        data: {
                            username: codeData.username,
                            contactMethod: "phone",
                        }
                    })
                }).catch((anotherErr) => {
                    NotificationUtil.sendSlackMessage("505: error during /help/reset_pass", "CF3U0LPDF");
                    res.status(202).json({
                        statusCode: 505,
                        timestamp: Date.now(),
                        message: "sending verification code failed",
                        data: anotherErr
                    });
                });
            }).catch((error) => {
                NotificationUtil.sendSlackMessage("505: error during /help/reset_pass", "CF3U0LPDF");
                res.status(202).json({
                    statusCode: 505,
                    timestamp: Date.now(),
                    message: "creating verification code failed",
                    data: error
                });
            });
        }
    });
});

module.exports = router;
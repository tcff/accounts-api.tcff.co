require('dotenv').config();

const axios = require('axios');
const IPINFO_TOKEN = process.env.IPINFO_TOKEN;

module.exports = {
    getIPData: (ipAddress) => {
        return new Promise((resolve, error) => {
            axios.get("https://ipinfo.io/"+ipAddress+"?token="+IPINFO_TOKEN).then((response) => {
                console.log(response.data);
                resolve(response.data);
            }).catch((error) => {
                error(error);
            });
        });
    }
};
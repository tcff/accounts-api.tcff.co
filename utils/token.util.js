require('dotenv').config();

const jwt = require("jsonwebtoken");
const SECRET = process.env.TCF_ACCOUNTS_SECRET;

const NotificationUtil = require("./notification.util");

module.exports = {
    generateSecureJWT: (username, email, fname, lname) => {
        NotificationUtil.sendSlackMessage("4: generating authToken from usernmae: "+username, "CF3U0LPDF");
        return new Promise((resolve, error) => {
            jwt.sign({
                timestamp: Date.now(),
                iss: "The Creative Foundation",
                exp: 15552000,
                data: {
                    username: username,
                    email: email,
                    meta: {
                        firstName: fname,
                        lastName: lname
                    }
                }
            }, SECRET, (err, authToken) => {
                if (err) {
                    error(err);
                }
                else {
                    resolve(authToken);
                }
            });
        });
    },
    decodeSecureJWT: (token) => {
        NotificationUtil.sendSlackMessage("5: decoding authToken for username: "+username, "CF3U0LPDF");
        return new Promise((resolve, error) => {
            jwt.verify(token, SECRET, (err, decodedAuthToken) => {
                if (err) {
                    error(err);
                }
                else {
                    resolve(decodedAuthToken);
                }
            });
        });
    }
}
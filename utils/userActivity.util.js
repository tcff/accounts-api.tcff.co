const UserActivityLogModel = require("../models/userActivityLog.model");
const IPDataUtil = require('./ipData.util');

module.exports = {
    logActivity: (type, ipAddress, username) => {
        IPDataUtil.getIPData().then((ipData) => {
            UserActivityLogModel({
                ipData: ipData,
                activityType: type,
                username: username,
                timestamp: Data.now()
            }).save((err, log) => {
                if (err) {
                    return 0;
                }
                else {
                    return 1;
                }
            });
        }).catch((error) => {
            return 0;
        });
    }
};
require('dotenv').config();

const CognitoExpress = require("cognito-express");
const jwt = require("jwt-simple");
const CLIENT_SECRET = process.env.TCF_ACCOUNTS_SECRET;

const cognitoExpress = new CognitoExpress({
    region: "us-east-2",
    cognitoUserPoolId: "us-east-2_9EdxvOGQP",
    tokenUse: "id",
    tokenExpiration: 3600000
});

module.exports = {
  authenticate: (token) => {
    return new Promise((resolve, error) => {
      cognitoExpress.validate(token, (err, response) => {
          if (err) {
            console.log(err);
              error({
                code: 0,
                response: {
                  statusCode: 401,
                  timestamp: Date.now(),
                  message: "error"
                }
              });
          }
          else {
            resolve({
              code: 1,
              user: response
            });
          }
        });
      });
    },
    authenticateApp: (token) => {
      const payload = jwt.decode(token, CLIENT_SECRET);
      return new Promise((resolve, error) => {
          if (payload.iss === "The Creative Foundation") {
              resolve({
                  authenticated: true,
                  message: "client is a TCF application"
              });
          }
          else {
              error({
                  authenticated: false,
                  message: "not a TCF application"
              })
          }
      });
    }
};

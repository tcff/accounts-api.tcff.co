require('dotenv').config();

const { WebClient } = require('@slack/client');
const slackToken = process.env.SLACK_TOKEN;
const web = new WebClient(slackToken);
const apiKey = process.env.MAILGUN_API_KEY;
const mailgun = require("mailgun-js")({apiKey: apiKey, domain: "mailgun.tcff.co"});

module.exports = {
    sendSlackMessage: (message, channel) => {
        web.chat.postMessage({
            channel: channel,
            text: message
        }).then((res) => {
            console.log('Slack message sent: ', res.ts);
            return 1;
        }).catch((error) => {
            console.log(error);
            return 0;
        });
    },
    sendEmail: (htmlBody, recipient, subjectLine) => {
        mailgun.messages().send({
            to: recipient,
            from: "Jojo the Unicorn <jojo@tcff.co>",
            subject: subjectLine,
            html: htmlBody
        }, (err, response) => {
            if (err) {
                return 0;
            }
            else {
                return 1;
            }
        });
    }
};

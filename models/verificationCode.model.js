const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VerificationCodeSchema = new Schema({
    username: String,
    code: String
});

VerificationCodeSchema.index({code: 1}, {unique: true});

module.exports = mongoose.model('VerificationCode', VerificationCodeSchema, 'verificationCodes');

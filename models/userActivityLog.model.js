const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserActivityLogSchema = new Schema({
    ipData: {
        type: String,
        strict: false
    },
    activityType: String,
    timestamp: String,
    username: String
});

module.exports = mongoose.model('UserActivityLog', UserActivityLogSchema, 'userActivityLogs');

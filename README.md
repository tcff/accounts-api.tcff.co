# TCF Accounts API

The API for the TCF Accounts Application to securely authenticate and verify users.

## Features Covered by TCF Accounts

- Logging in
- Registration
- Recovering a forgotten username using an email
- Account verification using phone or email upon registration
- 2FA authentication using email or phone by default on log ins
- Password reset using phone or email to verify account holder
- Request verification for Creative Services that need TCF Accounts
- Email updates on log ins and password resets
- Server to Server authentication
- The ability to edit and add information to a user's account

## Enviroment/Protected Variables

- **TCF_ACCOUNTS_SECRET**: Used to encrypt JWT tokens sent back
- **TWILIO_AUTH_TOKEN**: To authenticate the application with Twilio SDK 
- **TWILIO_ACCOUNT_SID**: For Twilio SDK
- **MAILGUN_API_KEY**: To authenticate the applicatio with the Mailgun API
- **MONGO_URL**: To securely connect to the Mongo DB database
- **SLACK_TOKEN**: Authentication API key for tcffco.slack.com workspace
## Flows

### A Member Signs Up

- accounts.tcff.co checks that a the username is not taken and the email is not in use
- accounts.tcff.co sends all data to accounts-api.tcff.co/auth/signup
- the signup route creates an unverified user with the given information
- the signup route creates a verification code and sends it to the user's phone number
- once the user has the code and input it, accounts.tcff.co sends the code to accouunts.tcff.co/verification/verify
- if the code is correct, the verify method will update the user to be verified, and will then generate an auth token
- accounts.tcff.co will route the user back to their application with the token as a query parameter

### A Member Needs Signs In

### A Member Needs To Recover Their Username

### A Member Needs To Reset Their Password



## Routes

### /api/signin

* UserNotConfirmedException (not verified)
    - user_not_confirmed
* NotAuthorizedException (Incorrect username or password)
    - incorrect_credentials
* UserNotFoundException (Username doesnt exist, therefore no user)
    - username_doesnt_exist

### /api/signup

* UsernameExistsException
    - username_exists
* MongoError, Code: 11000 (email is taken if it contains "email")
    - email_in_use

### /api/signup/verify

* NotAuthorizedException (user is already confirmed)
    - already_confirmed
* CodeMismatchException (wrong code)
    - wrong_code
    
### /api/resetpass/start

* UserNotFoundException
    - username_doesnt_exist

### /api/resetpass/confirm

* ExpiredCodeException (code expired and invalid)
     - expired_code
* CodeMismatchException (wrong code)
    - wrong_code
 